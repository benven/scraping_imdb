<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FILM</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Oi&display=swap" rel="stylesheet">
    <style>
        body{
            background-color: black;
            color: white;
            font-family: Arial, Helvetica, sans-serif;
        }

        h1 {
            font-family: 'Oi', cursive;;
            font-weight: 200;
            
            
        }

        div {
            width: 300px;
            padding: 5%;
            margin: 0 auto;
            text-align: center;
        }
    </style>
</head>
<body>
    <?php 
    if(isset($_GET['id'])){
      
        //on capture l'option id passé dans l'url, il s'agit de l'identifiant du film
        $id = $_GET['id'];
       
        $url = 'https://www.imdb.com/title/' . $id;
        //On recupère toute la page via la fonction file_get_content et en précisant l'url
        $source_film = file_get_contents($url);
        
        $text = htmlentities($source_film);
        
        //Ensuite, on applique un regex permettan de filtrer le nom

        preg_match_all('/<title>(.*)<\/title>/isU',$source_film, $name);
        //grosses difficultés sur le regex, ce qui marche sur RegexOne ne marche pas en PHP.
        // preg_match('/<title>(.*([.\w:]+?)\s)*\((\d*)\)/', $source_film, $nomEtAnnee);
        // preg_match_all('/<div class="[^"]*?originalTitle[^"]*?">(.*?)<\/div>/gm', $text, $titre);
        
    }
     

    echo '<div>';
    echo('<h1>');
    echo $name[1][0];
    echo('</h1>');
    echo '</div>';
        
    ?>
    
</body>
</html>